#include "SettingsState.hpp"
#include "Utility.hpp"
#include "ResourceHolder.hpp"

#include <SFML/Graphics/RenderWindow.hpp>


SettingsState::SettingsState(StateStack& stack, Context context)
	: State(stack, context)
	, mGUIContainer()
{
	mBackgroundSprite.setTexture(context.textures->get(TextureIDs::TitleScreen));

	// Build key binding buttons and labels
	addButtonLabel(Player::Action::P1MoveLeft, 120.f, 200.f, "P1 - Move Left", context);
	addButtonLabel(Player::Action::P1MoveRight, 120.f, 250.f, "P1 - Move Right", context);
	addButtonLabel(Player::Action::P1MoveUp, 120.f, 300.f, "P1- Move Up", context);
	addButtonLabel(Player::Action::P1MoveDown, 120.f, 350.f, "P1 - Move Down", context);
	addButtonLabel(Player::Action::P1Fire, 120.f, 450.f, "P1 - Fire", context);

	addButtonLabel(Player::Action::P2MoveLeft, 620.f, 200.f, "P2 - Move Left", context);
	addButtonLabel(Player::Action::P2MoveRight, 620.f, 250.f, "P2 - Move Right", context);
	addButtonLabel(Player::Action::P2MoveUp, 620.f, 300.f, "P2 - Move Up", context);
	addButtonLabel(Player::Action::P2MoveDown, 620.f, 350.f, "P2 - Move Down", context);
	addButtonLabel(Player::Action::P2Fire, 620.f, 450.f, "P2 - Fire", context);

	updateLabels();

	auto backButton = std::make_shared<GUI::Button>(context);
	backButton->setPosition(120.f, 600.f);
	backButton->setText("Back");
	backButton->setCallback(std::bind(&SettingsState::requestStackPop, this));

	mGUIContainer.pack(backButton);
}

void SettingsState::draw()
{
	sf::RenderWindow& window = *getContext().window;

	window.draw(mBackgroundSprite);
	window.draw(mGUIContainer);
}

bool SettingsState::update(sf::Time)
{
	return true;
}

bool SettingsState::handleEvent(const sf::Event& event)
{
	bool isKeyBinding = false;

	// Iterate through all key binding buttons to see if they are being pressed, waiting for the user to enter a key
	for (std::size_t action = 0; action < static_cast<int>(Player::Action::ActionCount); ++action)
	{
		if (mBindingButtons[action]->isActive())
		{
			isKeyBinding = true;
			if (event.type == sf::Event::KeyReleased)
			{
				getContext().player->assignKey(static_cast<Player::Action>(action), event.key.code);
				mBindingButtons[action]->deactivate();
			}
			break;
		}
	}

	// If pressed button changed key bindings, update labels; otherwise consider other buttons in container
	if (isKeyBinding)
		updateLabels();
	else
		mGUIContainer.handleEvent(event);

	return false;
}

void SettingsState::updateLabels()
{
	Player& player = *getContext().player;

	for (std::size_t i = 0; i < static_cast<int>(Player::Action::ActionCount); ++i)
	{
		sf::Keyboard::Key key = player.getAssignedKey(static_cast<Player::Action>(i));
		mBindingLabels[i]->setText(toString(key));
	}
}

void SettingsState::addButtonLabel(Player::Action action, float x, float y, const std::string& text, Context context)
{
	mBindingButtons[static_cast<int>(action)] = std::make_shared<GUI::Button>(context);
	mBindingButtons[static_cast<int>(action)]->setPosition(x, y);
	mBindingButtons[static_cast<int>(action)]->setText(text);
	mBindingButtons[static_cast<int>(action)]->setToggle(true);

	mBindingLabels[static_cast<int>(action)] = std::make_shared<GUI::Label>("", *context.fonts);
	mBindingLabels[static_cast<int>(action)]->setPosition(x + 300.f, y + 15.f);

	mGUIContainer.pack(mBindingButtons[static_cast<int>(action)]);
	mGUIContainer.pack(mBindingLabels[static_cast<int>(action)]);
}
