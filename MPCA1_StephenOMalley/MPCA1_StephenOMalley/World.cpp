#include "World.hpp"
#include "Projectile.hpp"
#include "Pickup.hpp"
#include "TextNode.hpp"
#include "Utility.hpp"
#include "ParticleNode.hpp"
#include "PostEffect.hpp"
#include "SoundNode.hpp"
#include "Wall.hpp"

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML\Graphics\RectangleShape.hpp>

#include <iostream>

#include <algorithm>
#include <cmath>
#include <limits>

World::World(sf::RenderTarget& outputTarget, FontHolder& fonts, SoundPlayer& sounds, Level& level)
	: mTarget(outputTarget)
	, mSceneTexture()
	, mWorldView(outputTarget.getDefaultView())
	, mTextures()
	, mFonts(fonts)
	, mSounds(sounds)
	, mSceneGraph()
	, mSceneLayers()
	, mWorldBounds(0.f, 0.f, mWorldView.getSize().x, 768.f)
	, mSpawnPositionPlayer1(70.f, 70.f)//mWorldView.getSize().x / 2.f, mWorldBounds.height - mWorldView.getSize().y / 2.f)
	, mSpawnPositionPlayer2(1250.f, 700.f)
	, mSpawnPoint3(1250.f, 70.f)
	, mSpawnPoint4(70.f, 700.f)
	, mSpawnPoints()
	, mPlayer1(nullptr)
	, mPlayer2(nullptr)
	, mLevel()
	, mP1Score(0)
	, mP2Score(0)
	, mRespawnPlayer(false)
	, mScore(nullptr)
	, mCollisionLeft(false)
	, mCollisionRight(false)
	, mCollisionDown(false)
	, mCollisionUp(false)
	, mClock()
{
	mSpawnPoints.push_back(mSpawnPositionPlayer1);
	mSpawnPoints.push_back(mSpawnPositionPlayer2);
	mSpawnPoints.push_back(mSpawnPoint3);
	mSpawnPoints.push_back(mSpawnPoint4);

	mSceneTexture.create(mTarget.getSize().x, mTarget.getSize().y);
	mLevel = static_cast<int>(level.GetCurrentLevel());
	//std::cout << "Level: " << static_cast<int>(level.GetCurrentLevel()) << std::endl;


	loadTextures();
	buildScene();
}

void World::update(sf::Time dt)
{
	// reset player velocity
	mPlayer1->setVelocity(0.f, 0.f);
	mPlayer2->setVelocity(0.f, 0.f);

	// Setup commands to destroy entities
	destroyEntitiesOutsideView();

	// Forward commands to scene graph
	while (!mCommandQueue.isEmpty())
		mSceneGraph.onCommand(mCommandQueue.pop(), dt);

	// Collision detection and response (may destroy entities)
	handleCollisions();

	// Remove all destroyed entities, create new ones
	mSceneGraph.removeWrecks();

	// Regular update step, adapt position (correct if outside view)
	mSceneGraph.update(dt, mCommandQueue);
	adaptPlayerPosition();
	updateSounds();

	// Check if any players died and make them respawn
	mRespawnPlayer ? respawnPlayer() : checkPlayerStatus();
}

void World::draw()
{
	/*if (PostEffect::isSupported() && mTarget.getc)
	{
		mSceneTexture.clear();
		mSceneTexture.setView(mWorldView);
		mSceneTexture.draw(mSceneGraph);
		mSceneTexture.display();
		mBloomEffect.apply(mSceneTexture, mTarget);
	}
	else*/
	{
		mTarget.setView(mWorldView);
		mTarget.draw(mSceneGraph);
	}
}

CommandQueue& World::getCommandQueue()
{
	return mCommandQueue;
}

bool World::isGameOver()
{
	if (mPlayer1->getLives() == 0 || mPlayer2->getLives() == 0)
		return true;
	else
		return false;
}

bool World::player1Wins()
{
	return mPlayer1->getLives() > 0;
}

bool World::hasAlivePlayer() const
{
	//return !mPlayer1->lifeLosted();
	return !mPlayer1->isDestroyed();
}

void World::checkPlayerStatus()
{
	if (mPlayer1->isDestroyed())
	{
		//mPlayer1->lifeLosted();
		mRespawnPlayer = true;
	}
	else if (mPlayer2->isDestroyed())
	{
		//mPlayer2->lifeLosted();
		mRespawnPlayer = true;
	}
}

void World::loadTextures()
{
	mTextures.load(TextureIDs::Pickups, "Media/Textures/pickups.png");
	mTextures.load(TextureIDs::Level1, "Media/Textures/Level1.png");
	mTextures.load(TextureIDs::Level2, "Media/Textures/Level2.png");
	mTextures.load(TextureIDs::Level3, "Media/Textures/Level3.png");
	mTextures.load(TextureIDs::Players, "Media/Textures/Players.png");
	mTextures.load(TextureIDs::Explosion, "Media/Textures/Explosion.png");
	mTextures.load(TextureIDs::Particle, "Media/Textures/Particle.png");
}

void World::adaptPlayerPosition()
{
	// Keep player's position inside the screen bounds, at least borderDistance units from the border
	sf::FloatRect viewBounds = getViewBounds();
	const float borderDistanceRight = 65.f;
	const float borderDistance = 40.f;

	sf::Vector2f positionP1 = mPlayer1->getPosition();
	positionP1.x = std::max(positionP1.x, viewBounds.left + borderDistanceRight);
	positionP1.x = std::min(positionP1.x, viewBounds.left + viewBounds.width - borderDistanceRight);
	positionP1.y = std::max(positionP1.y, viewBounds.top + borderDistanceRight);
	positionP1.y = std::min(positionP1.y, viewBounds.top + viewBounds.height - borderDistance);
	mPlayer1->setPosition(positionP1);

	sf::Vector2f positionP2 = mPlayer2->getPosition();
	positionP2.x = std::max(positionP2.x, viewBounds.left + borderDistanceRight);
	positionP2.x = std::min(positionP2.x, viewBounds.left + viewBounds.width - borderDistanceRight);
	positionP2.y = std::max(positionP2.y, viewBounds.top + borderDistanceRight);
	positionP2.y = std::min(positionP2.y, viewBounds.top + viewBounds.height - borderDistance);
	mPlayer2->setPosition(positionP2);

	if (mPlayer1->getVelocity().x != 0.f && mPlayer1->getVelocity().y != 0.f)
		//mPlayer1->setVelocity(mPlayer1->getVelocity() / std::sqrt(2.f));
		mPlayer1->setVelocity(0, 0);

	if (mPlayer2->getVelocity().x != 0.f && mPlayer2->getVelocity().y != 0.f)
		mPlayer2->setVelocity(mPlayer2->getVelocity() / std::sqrt(2.f));
}

bool matchesCategories(SceneNode::Pair& colliders, Category type1, Category type2)
{
	unsigned int category1 = colliders.first->getCategory();
	unsigned int category2 = colliders.second->getCategory();

	// Make sure first pair entry has category type1 and second has type2
	if (static_cast<int>(type1) & category1 && static_cast<int>(type2) & category2)
	{
		return true;
	}
	else if (static_cast<int>(type1) & category2 && static_cast<int>(type2) & category1)
	{
		std::swap(colliders.first, colliders.second);
		return true;
	}
	else
	{
		return false;
	}
}

void World::handleCollisions()
{
	std::set<SceneNode::Pair> collisionPairs;
	mSceneGraph.checkSceneCollision(mSceneGraph, collisionPairs);

	for (SceneNode::Pair pair : collisionPairs)
	{
		if (matchesCategories(pair, Category::Player1, Category::Player2))
		{
			auto& player = static_cast<Commander&>(*pair.first);
			auto& enemy = static_cast<Commander&>(*pair.second);

			// Collision: Player damage = enemy's remaining HP

			mElapsedTime = mClock.getElapsedTime();

			if (mElapsedTime.asSeconds() >= 1)
			{
				player.damage(1);
				enemy.damage(1);
				mClock.restart(); 
			}

				// Left
			//if (player.getVelocity().x < 0.f) {
			//	player.setVelocity(0, player.getMaxSpeed());
			//}

			////Right
			//else if (player.getVelocity().x > 0.f) {
			//	player.setVelocity(0, player.getMaxSpeed());
			//}

			//// Face Up
			//else if (player.getVelocity().y < 0.f) {
			//	player.setVelocity(player.getMaxSpeed(), 0);
			//}
			////Face Down
			//else if (player.getVelocity().y > 0.f) {
			//	player.setVelocity(player.getMaxSpeed(), 0);
			//}
			//// Left
			//if (enemy.getVelocity().x < 0.f)
			//	enemy.setVelocity(0, player.getMaxSpeed());

			////Right
			//else if (enemy.getVelocity().x > 0.f)
			//	enemy.setVelocity(0, player.getMaxSpeed());

			//// Face Up
			//if (enemy.getVelocity().y < 0.f)
			//	enemy.setVelocity(player.getMaxSpeed(), 0);
			////Face Down
			//else if (enemy.getVelocity().y > 0.f)
			//	enemy.setVelocity(player.getMaxSpeed(), 0);
			//player.setVelocity(0, 0);
			////damage(enemy.getHitpoints());
			//enemy.setVelocity(0, 0);
			/*std::cout << "Collision! " << std::endl;*/
		}

		else if (matchesCategories(pair, Category::Player1, Category::Wall) ||
			matchesCategories(pair, Category::Player2, Category::Wall))
		{
			auto& player = static_cast<Commander&>(*pair.first);
			auto& wall = static_cast<Wall&>(*pair.second);

			
			//sf::Vector2f playerLocation = player.getPosition(); 

			//try Collision Detect with sprite dir & velocity

			if (player.getVelocity().x > 0.f)
				mCollisionRight = true;
			if (player.getVelocity().x < 0.f)
				mCollisionLeft = true;
			if (player.getVelocity().y > 0.f)
				mCollisionDown = true;
			if (player.getVelocity().y < 0.f)
				mCollisionUp = true;

			if (mCollisionLeft)
			{
				std::cout << "Collision Left! " << std::endl;
				//player.setPosition(playerLocation.x + 5.f, playerLocation.y);
				//player.setVelocityX(0);

				player.getVelocity().x < 0.f ? player.setVelocityX(0.f), player.setPosition(player.getPosition().x + 15.f, player.getPosition().y) : player.setVelocityX(5.f);

				/*player.setVelocityX(0);
				if (player.getVelocity().x < 0.f)
					player.setVelocityX(player.getMaxSpeed());
				else
					player.setVelocityX(0);*/
				mCollisionLeft = false;
			}
			if (mCollisionRight)
			{
				std::cout << "Collision Right! " << std::endl;
				//player.setPosition(playerLocation.x - 5.f, playerLocation.y);
				//player.setVelocityX(0);


				player.getVelocity().x > 0.f ? player.setVelocityX(0.f), player.setPosition(player.getPosition().x - 15.f, player.getPosition().y) : player.setVelocityX(-5.f);

				mCollisionRight = false;

			}
			if (mCollisionDown)
			{
				std::cout << "Collision Down! " << std::endl;
				//player.setPosition(playerLocation.x, playerLocation.y - 5.f);
				
				player.getVelocity().y > 0.f ? player.setVelocityY(0.f), player.setPosition(player.getPosition().x, player.getPosition().y - 15.f) : player.setVelocityY(-5.f);

				/*player.setVelocityY(0);*/
				mCollisionDown = false;

			}
			if (mCollisionUp)
			{
				std::cout << "Collision Up! " << std::endl;
				//player.setPosition(playerLocation.x, playerLocation.y + 5.f);

				player.getVelocity().y < 0.f ? player.setVelocityY(0.f) ,player.setPosition(player.getPosition().x, player.getPosition().y + 15.f) : player.setVelocityY(5.f);

				/*player.setVelocityY(0);*/
				mCollisionUp = false;
			}
		}

		else if (matchesCategories(pair, Category::AlliedProjectile, Category::Wall) ||
			matchesCategories(pair, Category::EnemyProjectile, Category::Wall))
		{
			auto& bullet = static_cast<Projectile&>(*pair.first);
			auto& wall = static_cast<Wall&>(*pair.second);

			bullet.destroy();
		}

		else if (matchesCategories(pair, Category::Player1, Category::Pickup) ||
			matchesCategories(pair, Category::Player2, Category::Pickup))
		{
			auto& player = static_cast<Commander&>(*pair.first);
			auto& pickup = static_cast<Pickup&>(*pair.second);

			// Apply pickup effect to player, destroy projectile
			pickup.apply(player);
			pickup.destroy();
			player.playLocalSound(mCommandQueue, SoundEffectIDs::CollectPickup);
		}

		else if (matchesCategories(pair, Category::Player1, Category::EnemyProjectile)
			|| matchesCategories(pair, Category::Player2, Category::AlliedProjectile))
		{
			auto& commander = static_cast<Commander&>(*pair.first);
			auto& projectile = static_cast<Projectile&>(*pair.second);

			// Apply projectile damage to aircraft, destroy projectile
			commander.damage(projectile.getDamage());
			projectile.destroy();
		}
	}
}

void World::updateSounds()
{
	// Set listener's position to player position
	mSounds.setListenerPosition(mPlayer1->getWorldPosition());

	// Remove unused sounds
	mSounds.removeStoppedSounds();
}

void World::respawnPlayer(/*Commander* player*/)
{
	if (mPlayer1->hasDied())
	{
		mPlayer1->lifeLosted();
		mPlayer1->setPosition(mSpawnPoints[randomInt(4)]);
		mPlayer1->setSpawnHealth();
		mP2Score++;
	}
	else if (mPlayer2->hasDied())
	{
		mPlayer2->lifeLosted();
		mPlayer2->setPosition(mSpawnPoints[randomInt(4)]);
		mPlayer2->setSpawnHealth();
		mP1Score++;
	}

	mScore->setString(std::to_string(mP1Score) + " - " + std::to_string(mP2Score));

	mRespawnPlayer = false;
}

void World::buildScene()
{
	// Initialize the different layers
	for (std::size_t i = 0; i < Layer::LayerCount; ++i)
	{
		Category category = (i == Layer::LowerAir) ? Category::SceneAirLayer : Category::None;

		SceneNode::Ptr layer(new SceneNode(category));
		mSceneLayers[i] = layer.get();

		mSceneGraph.attachChild(std::move(layer));
	}

	sf::Texture& levelTexture = mTextures.get(TextureIDs::Level3);

	switch (mLevel)
	{
	case 0:
		levelTexture = mTextures.get(TextureIDs::Level1);
		createWalls(sf::Vector2f(20, 100), sf::Vector2f(200, 150));
		createWalls(sf::Vector2f(200, 20), sf::Vector2f(200, 150));

		createWalls(sf::Vector2f(20, -100), sf::Vector2f(200, 618));
		createWalls(sf::Vector2f(200, 20), sf::Vector2f(200, 618));

		createWalls(sf::Vector2f(20, 100), sf::Vector2f(1146, 150));
		createWalls(sf::Vector2f(200, 20), sf::Vector2f(966, 150));

		createWalls(sf::Vector2f(20, 100), sf::Vector2f(1146, 518));
		createWalls(sf::Vector2f(200, 20), sf::Vector2f(966, 618));


		createWalls(sf::Vector2f(20, 100), sf::Vector2f((mWorldBounds.width / 2.f) + 100.f, (mWorldBounds.height / 2.f) + 80.f));
		createWalls(sf::Vector2f(150, 20), sf::Vector2f((mWorldBounds.width / 2.f) + 100.f , (mWorldBounds.height / 2.f) + 80.f));

		createWalls(sf::Vector2f(20, 100), sf::Vector2f((mWorldBounds.width / 2.f) - 120.f, (mWorldBounds.height / 2.f) + 80.f));
		createWalls(sf::Vector2f(-150, 20), sf::Vector2f((mWorldBounds.width / 2.f) - 100.f, (mWorldBounds.height / 2.f) + 80.f));

		createWalls(sf::Vector2f(20, -100), sf::Vector2f((mWorldBounds.width / 2.f) - 120.f, (mWorldBounds.height / 2.f) - 80.f));
		createWalls(sf::Vector2f(-150, 20), sf::Vector2f((mWorldBounds.width / 2.f) - 100.f, (mWorldBounds.height / 2.f) - 80.f));

		createWalls(sf::Vector2f(20, -100), sf::Vector2f((mWorldBounds.width / 2.f) + 100.f, (mWorldBounds.height / 2.f) - 80.f));
		createWalls(sf::Vector2f(150, 20), sf::Vector2f((mWorldBounds.width / 2.f) + 100.f, (mWorldBounds.height / 2.f) - 80.f));


		break;
	case 1:
		levelTexture = mTextures.get(TextureIDs::Level2);

		createWalls(sf::Vector2f(900, 20), sf::Vector2f(200, 150));
		createWalls(sf::Vector2f(900, 20), sf::Vector2f(200, 618));

		createWalls(sf::Vector2f(20, 268), sf::Vector2f(200, 264));
		createWalls(sf::Vector2f(20, 268), sf::Vector2f(1066, 264));

		createWalls(sf::Vector2f(60, 168), sf::Vector2f((mWorldBounds.width / 2.f) - 30.f, 314));



		break;
	case 2:
		levelTexture = mTextures.get(TextureIDs::Level3);
		
		createWalls(sf::Vector2f(20, 150), sf::Vector2f(200, 20));
		createWalls(sf::Vector2f(20, 300), sf::Vector2f(200, 264));
		createWalls(sf::Vector2f(20, -100), sf::Vector2f(200, 761));

		createWalls(sf::Vector2f(20, 150), sf::Vector2f(1066, 20));
		createWalls(sf::Vector2f(20, 300), sf::Vector2f(1066, 264));
		createWalls(sf::Vector2f(20, -100), sf::Vector2f(1066, 761));

		createWalls(sf::Vector2f(20, 100), sf::Vector2f((mWorldBounds.width / 2.f) - 200.f, (mWorldBounds.height / 2.f) - 150.f));
		createWalls(sf::Vector2f(150, 20), sf::Vector2f((mWorldBounds.width / 2.f) - 200.f, (mWorldBounds.height / 2.f) - 150.f));

		createWalls(sf::Vector2f(20, 100), sf::Vector2f((mWorldBounds.width / 2.f) + 200.f, (mWorldBounds.height / 2.f) - 150.f));
		createWalls(sf::Vector2f(-150, 20), sf::Vector2f((mWorldBounds.width / 2.f) + 220.f, (mWorldBounds.height / 2.f) - 150.f));

		createWalls(sf::Vector2f(20, -100), sf::Vector2f((mWorldBounds.width / 2.f) - 200.f, (mWorldBounds.height / 2.f) + 150.f));
		createWalls(sf::Vector2f(150, 20), sf::Vector2f((mWorldBounds.width / 2.f) - 200.f, (mWorldBounds.height / 2.f) + 150.f));

		createWalls(sf::Vector2f(20, -100), sf::Vector2f((mWorldBounds.width / 2.f) + 200.f, (mWorldBounds.height / 2.f) + 150.f));
		createWalls(sf::Vector2f(-150, 20), sf::Vector2f((mWorldBounds.width / 2.f) + 220.f, (mWorldBounds.height / 2.f) + 150.f));

		break;

	default:
		break;
	}

	std::unique_ptr<SpriteNode> levelSprite(new SpriteNode(levelTexture));
	levelSprite->setPosition(mWorldBounds.left, mWorldBounds.top);
	mSceneLayers[Background]->attachChild(std::move(levelSprite));

	// Add particle node to the scene
	std::unique_ptr<ParticleNode> smokeNode(new ParticleNode(Particle::Type::Smoke, mTextures));
	mSceneLayers[LowerAir]->attachChild(std::move(smokeNode));

	// Add exhaust particle node to the scene
	std::unique_ptr<ParticleNode> exhaustNode(new ParticleNode(Particle::Type::Exhaust, mTextures));
	mSceneLayers[LowerAir]->attachChild(std::move(exhaustNode));

	//Add TextNode
	std::unique_ptr<TextNode> scoreDisplay(new TextNode(mFonts, ""));
	scoreDisplay->setPosition(mWorldBounds.width / 2.f, mWorldBounds.height / 2.f);
	scoreDisplay->setScale(3.f, 3.f);
	scoreDisplay->setString("0 - 0");
	mScore = scoreDisplay.get();
	mSceneLayers[LowerAir]->attachChild(std::move(scoreDisplay));

	//Add sound effect node
	std::unique_ptr<SoundNode> soundNode(new SoundNode(mSounds));
	mSceneGraph.attachChild(std::move(soundNode));

	// PLayer 1
	std::unique_ptr<Commander> player1(new Commander(Commander::Type::Player1, mTextures, mFonts));
	mPlayer1 = player1.get();
	mPlayer1->setPosition(mSpawnPositionPlayer1);
	mSceneLayers[Layer::UpperAir]->attachChild(std::move(player1));
	// PLayer 2
	std::unique_ptr<Commander> player2(new Commander(Commander::Type::Player2, mTextures, mFonts));
	mPlayer2 = player2.get();
	mPlayer2->setPosition(mSpawnPositionPlayer2);
	mSceneLayers[Layer::UpperAir]->attachChild(std::move(player2));
}

void World::createWalls(sf::Vector2f size, sf::Vector2f position)
{ 

	std::unique_ptr<Wall> wall(new Wall(size, position));
	mSceneLayers[Layer::UpperAir]->attachChild(std::move(wall));
}

void World::destroyEntitiesOutsideView()
{
	Command command;
	command.category = static_cast<int>(Category::Projectile);
	command.action = derivedAction<Entity>([this](Entity& e, sf::Time)
	{
		if (!getBattlefieldBounds().intersects(e.getBoundingRect()))
			e.destroy();
	});

	mCommandQueue.push(command);
}

sf::FloatRect World::getViewBounds() const
{
	return sf::FloatRect(mWorldView.getCenter() - mWorldView.getSize() / 2.f, mWorldView.getSize());
}

sf::FloatRect World::getBattlefieldBounds() const
{
	// Return view bounds + some area at top, where enemies spawn
	sf::FloatRect bounds = getViewBounds();
	bounds.top -= 100.f;
	bounds.height += 100.f;

	return bounds;
}