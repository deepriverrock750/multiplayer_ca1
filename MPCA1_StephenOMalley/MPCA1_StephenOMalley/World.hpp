#pragma once
#include "ResourceHolder.hpp"
#include "ResourceIdentifiers.hpp"
#include "SceneNode.hpp"
#include "SpriteNode.hpp"
#include "Commander.hpp"
#include "Command.hpp"
#include "CommandQueue.hpp"
#include "BloomEffect.hpp"
#include "SoundPlayer.hpp"
#include "Level.hpp"

#include "SFML/System/NonCopyable.hpp"
#include "SFML/Graphics/View.hpp"
#include "SFML/Graphics/Texture.hpp"
#include "SFML\System\Clock.hpp"

#include <array>
#include <queue>

//Forward declaration
namespace sf
{
	class RenderTarget;
}

class World : private sf::NonCopyable {
public:
	explicit World(sf::RenderTarget& outputTarget, FontHolder& fonts, SoundPlayer& sounds, Level& level);
	void update(sf::Time dt);
	void draw();

	CommandQueue& getCommandQueue();

	bool isGameOver();
	bool player1Wins();
	bool hasAlivePlayer() const;
	void checkPlayerStatus();

private:
	void loadTextures();
	void buildScene();
	void adaptPlayerPosition();
	void handleCollisions();

	void createWalls(sf::Vector2f size, sf::Vector2f position);
	void updateSounds();
	void respawnPlayer(/*Commander* player*/);

	void destroyEntitiesOutsideView();
	sf::FloatRect getViewBounds() const;
	sf::FloatRect getBattlefieldBounds() const;

private:
	enum Layer{Background, LowerAir, UpperAir, LayerCount};

private:
	sf::RenderTarget& mTarget;
	sf::RenderTexture mSceneTexture;
	sf::View mWorldView;
	TextureHolder mTextures;
	FontHolder&	mFonts;
	SoundPlayer& mSounds;

	SceneNode mSceneGraph;
	std::array<SceneNode*, static_cast<int>(Layer::LayerCount)> mSceneLayers;
	CommandQueue mCommandQueue;

	sf::FloatRect mWorldBounds;
	sf::Vector2f mSpawnPositionPlayer1;
	sf::Vector2f mSpawnPositionPlayer2;
	sf::Vector2f mSpawnPoint3;
	sf::Vector2f mSpawnPoint4;

	bool mCollisionLeft;
	bool mCollisionRight;
	bool mCollisionUp;
	bool mCollisionDown;

	std::vector<sf::Vector2f> mSpawnPoints;
	Commander* mPlayer1;
	Commander* mPlayer2;
	int mLevel;
	bool mRespawnPlayer;
	int mP1Score;
	int mP2Score;
	sf::Clock mClock;
	sf::Time mElapsedTime;

	BloomEffect	mBloomEffect;
	TextNode* mScore;
};