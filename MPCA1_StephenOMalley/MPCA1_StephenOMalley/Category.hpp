#pragma once
//Entity/Scene node category, used to dispatch messages

enum class Category {
	None = 0,
	SceneAirLayer = 1 << 0,
	Player1 = 1 << 1,
	Player2 = 1 << 2,
	Pickup = 1 << 3,
	AlliedProjectile = 1 << 4,
	EnemyProjectile = 1 << 5,
	ParticleSystem = 1 << 6,
	SoundEffect = 1 << 7,
	Wall = 1 << 8,

	Commander = Player1 | Player2,
	Projectile = AlliedProjectile | EnemyProjectile
};