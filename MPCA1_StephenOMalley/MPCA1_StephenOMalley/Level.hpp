#pragma once
#include "ResourceIdentifiers.hpp"

#include <SFML/Graphics/Export.hpp>
#include <SFML/Graphics/Shape.hpp>
class Level
{

public:
	//enum class LevelIDs {Level1, Level2, Level3, TypeCount };

	Level();
	//void CreateLevel();

	 void SetCurrentLevel(LevelIDs level);
	 LevelIDs GetCurrentLevel();

private:
	LevelIDs mCurrentLevel;
};
