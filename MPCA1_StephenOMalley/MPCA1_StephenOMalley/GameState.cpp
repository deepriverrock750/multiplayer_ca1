#include "GameState.hpp"
#include <SFML/Graphics/RenderWindow.hpp>
#include "MusicPlayer.hpp"

#include <iostream>


GameState::GameState(StateStack& stack, Context context)
	:State(stack, context)
	, mWorld(*context.window, *context.fonts, *context.sounds, *context.level)
	, mPlayer(*context.player)
	, mLevel(*context.level)
{
	mPlayer.setMissionStatus(Player::MissionStatus::MissionRunning);
	//mPlayer2.setMissionStatus(Player2::MissionStatus::MissionRunning);
	//mLevel.SetCurrentLevel(LevelIDs::Level2);
	std::cout << "Level: " << static_cast<int>( mLevel.GetCurrentLevel()) << std::endl;


	//Play the mission theme
	context.music->play(MusicIDs::MissionTheme);
}

void GameState::draw()
{
	mWorld.draw();
}

bool GameState::update(sf::Time dt)
{
	mWorld.update(dt);

	//if (!mWorld.hasAlivePlayer())
	if(mWorld.isGameOver())
	{
		mWorld.player1Wins() ? mPlayer.setMissionStatus(Player::MissionStatus::MissionSuccess) :
			mPlayer.setMissionStatus(Player::MissionStatus::MissionFailure);
		requestStackPush(StateIDs::GameOver);
	}

	CommandQueue& commands = mWorld.getCommandQueue();
	mPlayer.handleRealtimeInput(commands);

	return true;
}

bool GameState::handleEvent(const sf::Event & event)
{
	//Game input handling
	CommandQueue& commands = mWorld.getCommandQueue();
	mPlayer.handleEvent(event, commands);

	//Escape pressed, trigger pause screen
	if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape)
	{
		requestStackPush(StateIDs::Pause);
	}
	return true;
}


