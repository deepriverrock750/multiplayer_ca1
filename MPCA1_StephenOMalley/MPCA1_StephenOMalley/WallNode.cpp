#include "WallNode.hpp"
#include "Category.hpp"
#include "SFML/Graphics/RenderTarget.hpp"


WallNode::WallNode(sf::RectangleShape& wall)
	: Entity(1)
	, mWall()
{
}

unsigned int WallNode::getCategory() const
{
	return static_cast<int>(Category::Wall);
}

sf::FloatRect WallNode::getBoundingRect() const
{
	return getWorldTransform().transformRect(mWall.getGlobalBounds());
}

void WallNode::drawCurrent(sf::RenderTarget & target, sf::RenderStates states) const
{
	target.draw(mWall, states);
}
