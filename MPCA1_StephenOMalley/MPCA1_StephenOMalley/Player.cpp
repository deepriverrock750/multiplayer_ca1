#include "Player.hpp"
#include "CommandQueue.hpp"
#include "Commander.hpp" 

#include <map>
#include <string>
#include <algorithm>

using namespace std::placeholders;

struct CommanderMover
{
	CommanderMover(float vx, float vy) : velocity(vx, vy)
	{

	}
	void operator() (Commander& commander, sf::Time) const
	{
		commander.accelerate(velocity * commander.getMaxSpeed());
	}
	sf::Vector2f velocity;
};

Player::Player()
	: mCurrentMissionStatus(MissionStatus::MissionRunning)
{
	//Set initial key bindings
	mKeyBinding[sf::Keyboard::A] = Action::P1MoveLeft;
	mKeyBinding[sf::Keyboard::D] = Action::P1MoveRight;
	mKeyBinding[sf::Keyboard::W] = Action::P1MoveUp;
	mKeyBinding[sf::Keyboard::S] = Action::P1MoveDown;
	mKeyBinding[sf::Keyboard::Space] = Action::P1Fire;
	//mKeyBinding[sf::Keyboard::M] = Action::P1LaunchMissile;

	mKeyBindingPlayer2[sf::Keyboard::Left] = Action::P2MoveLeft;
	mKeyBindingPlayer2[sf::Keyboard::Right] = Action::P2MoveRight;
	mKeyBindingPlayer2[sf::Keyboard::Up] = Action::P2MoveUp;
	mKeyBindingPlayer2[sf::Keyboard::Down] = Action::P2MoveDown;
	mKeyBindingPlayer2[sf::Keyboard::RShift] = Action::P2Fire;
	//mKeyBindingPlayer2[sf::Keyboard::End] = Action::P2LaunchMissile;

	//set initial action bindings
	initializeActions();
	//Assign all categories to the player's aircraft
	for (auto& pair : mActionBinding)
	{
		pair.second.category = static_cast<unsigned int>(Category::Player1);
		//pair.second.category = static_cast<unsigned int>(Category::Player2);
	}
	for (auto& pair : mActionBindingPlayer2)
	{
		pair.second.category = static_cast<unsigned int>(Category::Player2);
	}
}

void Player::handleEvent(const sf::Event& event, CommandQueue& commands)
{
	if (event.type == sf::Event::KeyPressed)
	{
		//check if key pressed is in the key bindings, if so trigger command
		auto found = mKeyBinding.find(event.key.code);
		if (found != mKeyBinding.end() && !isRealtimeAction(found->second))
		{
			commands.push(mActionBinding[found->second]);
		}

		//check if key pressed is in the key bindings, if so trigger command
		auto found2 = mKeyBindingPlayer2.find(event.key.code);
		if (found2 != mKeyBindingPlayer2.end() && !isRealtimeAction(found2->second))
		{
			commands.push(mActionBindingPlayer2[found2->second]);
		}
	}
}

void Player::handleRealtimeInput(CommandQueue& commands)
{
	//Check if any key binding keys are pressed
	for (auto pair : mKeyBinding)
	{
		if (sf::Keyboard::isKeyPressed(pair.first) && isRealtimeAction(pair.second))
		{
			commands.push(mActionBinding[pair.second]);
		}
	}

	for (auto pair : mKeyBindingPlayer2)
	{
		if (sf::Keyboard::isKeyPressed(pair.first) && isRealtimeAction(pair.second))
		{
			commands.push(mActionBindingPlayer2[pair.second]);
		}
	}
}

void Player::assignKey(Action action, sf::Keyboard::Key key)
{
	//Remove all keys that are already mapped to an action
	for (auto itr = mKeyBinding.begin(); itr != mKeyBinding.end();)
	{
		if (itr->second == action)
		{
			mKeyBinding.erase(itr++);
		}
		else
		{
			++itr;
		}
		//insert new binding
		mKeyBinding[key] = action;
	}

	for (auto itr = mKeyBindingPlayer2.begin(); itr != mKeyBindingPlayer2.end();)
	{
		if (itr->second == action)
		{
			mKeyBindingPlayer2.erase(itr++);
		}
		else
		{
			++itr;
		}
		//insert new binding
		mKeyBindingPlayer2[key] = action;
	}
}

sf::Keyboard::Key Player::getAssignedKey(Action action) const
{
	for (auto pair : mKeyBinding)
	{
		if (pair.second == action)
		{
			return pair.first;
		}
	}

	for (auto pair : mKeyBindingPlayer2)
	{
		if (pair.second == action)
		{
			return pair.first;
		}
	}
	return sf::Keyboard::Unknown;
}

void Player::setMissionStatus(MissionStatus status)
{
	mCurrentMissionStatus = status;
}

Player::MissionStatus Player::getMissionStatus() const
{
	return mCurrentMissionStatus;
}

void Player::initializeActions()
{
	/*mActionBinding[Action::MoveLeft].action = derivedAction<Aircraft>(AircraftMover(-1, 0.f));
	mActionBinding[Action::MoveRight].action = derivedAction<Aircraft>(AircraftMover(1, 0.f));
	mActionBinding[Action::MoveUp].action = derivedAction<Aircraft>(AircraftMover(0.f, -1));
	mActionBinding[Action::MoveDown].action = derivedAction<Aircraft>(AircraftMover(0.f, 1));
	mActionBinding[Action::Fire].action = derivedAction<Aircraft>([](Aircraft& a, sf::Time) { a.fire(); });
	mActionBinding[Action::LaunchMissile].action = derivedAction<Aircraft>([](Aircraft& a, sf::Time) { a.launchMissile(); });*/

	mActionBinding[Action::P1MoveLeft].action = derivedAction<Commander>(CommanderMover(-1, 0.f));
	mActionBinding[Action::P1MoveRight].action = derivedAction<Commander>(CommanderMover(1, 0.f));
	mActionBinding[Action::P1MoveUp].action = derivedAction<Commander>(CommanderMover(0.f, -1));
	mActionBinding[Action::P1MoveDown].action = derivedAction<Commander>(CommanderMover(0.f, 1));
	mActionBinding[Action::P1Fire].action = derivedAction<Commander>([](Commander& a, sf::Time) { a.fire(); });
	//mActionBinding[Action::P1LaunchMissile].action = derivedAction<Commander>([](Commander& a, sf::Time) { a.launchMissile(); });

	mActionBindingPlayer2[Action::P2MoveLeft].action = derivedAction<Commander>(CommanderMover(-1, 0.f));
	mActionBindingPlayer2[Action::P2MoveRight].action = derivedAction<Commander>(CommanderMover(1, 0.f));
	mActionBindingPlayer2[Action::P2MoveUp].action = derivedAction<Commander>(CommanderMover(0.f, -1));
	mActionBindingPlayer2[Action::P2MoveDown].action = derivedAction<Commander>(CommanderMover(0.f, 1));
	mActionBindingPlayer2[Action::P2Fire].action = derivedAction<Commander>([](Commander& a, sf::Time) { a.fire(); });
	//mActionBindingPlayer2[Action::P2LaunchMissile].action = derivedAction<Commander>([](Commander& a, sf::Time) { a.launchMissile(); });
}

bool Player::isRealtimeAction(Action action)
{
	switch (action)
	{
	case Action::P1MoveLeft:
	case Action::P1MoveRight:
	case Action::P1MoveUp:
	case Action::P1MoveDown:
	case Action::P1Fire:
	case Action::P2MoveLeft:
	case Action::P2MoveRight:
	case Action::P2MoveUp:
	case Action::P2MoveDown:
	case Action::P2Fire:
		return true;
	default:
		return false;
	}
}

