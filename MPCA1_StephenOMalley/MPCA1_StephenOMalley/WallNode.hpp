#pragma once
#include "Entity.hpp"
#include "Command.hpp"
#include "ResourceIdentifiers.hpp"

#include "SFML/Graphics/RectangleShape.hpp"

class WallNode : public Entity
{
public:
	 WallNode( sf::RectangleShape& wall);
	 virtual unsigned int	getCategory() const;
	 virtual sf::FloatRect	getBoundingRect() const;

private:
	virtual void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;

private:
	sf::RectangleShape mWall;
};