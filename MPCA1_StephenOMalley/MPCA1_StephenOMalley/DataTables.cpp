#include "DataTables.hpp"
#include "Commander.hpp"
#include "Projectile.hpp"
#include "Particle.hpp"
#include "Pickup.hpp"
#include "Constants.hpp"

using namespace std::placeholders;

std::vector<PlayerData> initializePlayerData()
{
	std::vector<PlayerData> data(static_cast<int>(Commander::Type::TypeCount));
	data[static_cast<int>(Commander::Type::Player1)].hitpoints = PLAYERHEALTH;
	data[static_cast<int>(Commander::Type::Player1)].speed = 300.f;
	data[static_cast<int>(Commander::Type::Player1)].fireInterval = sf::seconds(1);
	data[static_cast<int>(Commander::Type::Player1)].texture = TextureIDs::Players;
	data[static_cast<int>(Commander::Type::Player1)].textureRect = sf::IntRect(0, 0, 86, 50);

	data[static_cast<int>(Commander::Type::Player2)].hitpoints = PLAYERHEALTH;
	data[static_cast<int>(Commander::Type::Player2)].speed = 300.f;
	data[static_cast<int>(Commander::Type::Player2)].fireInterval = sf::seconds(1);
	data[static_cast<int>(Commander::Type::Player2)].texture = TextureIDs::Players;
	data[static_cast<int>(Commander::Type::Player2)].textureRect = sf::IntRect(0, 50, 86, 50);

	return data;
}

std::vector<ProjectileData> initializeProjectileData()
{
	std::vector<ProjectileData> data(static_cast<int>(Projectile::ProjectileIDs::TypeCount));

	data[static_cast<int>(Projectile::ProjectileIDs::AlliedBullet)].damage = 15;
	data[static_cast<int>(Projectile::ProjectileIDs::AlliedBullet)].speed = 600.f;
	/*data[static_cast<int>(Projectile::ProjectileIDs::AlliedBullet)].texture = TextureIDs::Entities;
	data[static_cast<int>(Projectile::ProjectileIDs::AlliedBullet)].textureRect = sf::IntRect(175, 64, 3, 14);*/
	data[static_cast<int>(Projectile::ProjectileIDs::AlliedBullet)].texture = TextureIDs::Pickups;
	data[static_cast<int>(Projectile::ProjectileIDs::AlliedBullet)].textureRect = sf::IntRect(120, 0, 3, 14);

	data[static_cast<int>(Projectile::ProjectileIDs::EnemyBullet)].damage = 15;
	data[static_cast<int>(Projectile::ProjectileIDs::EnemyBullet)].speed = 600.f;
	/*data[static_cast<int>(Projectile::ProjectileIDs::EnemyBullet)].texture = TextureIDs::Entities;
	data[static_cast<int>(Projectile::ProjectileIDs::EnemyBullet)].textureRect = sf::IntRect(178, 64, 3, 14);*/
	data[static_cast<int>(Projectile::ProjectileIDs::EnemyBullet)].texture = TextureIDs::Pickups;
	data[static_cast<int>(Projectile::ProjectileIDs::EnemyBullet)].textureRect = sf::IntRect(123, 0, 3, 14);

	return data;
}

std::vector<PickupData> initializePickupData()
{
	std::vector<PickupData> data(static_cast<int>(Pickup::PickupID::TypeCount));

	/*data[static_cast<int>(Pickup::PickupID::HealthRefill)].texture = TextureIDs::Entities;
	data[static_cast<int>(Pickup::PickupID::HealthRefill)].textureRect = sf::IntRect(0, 64, 40, 40);*/
	data[static_cast<int>(Pickup::PickupID::HealthRefill)].texture = TextureIDs::Pickups;
	data[static_cast<int>(Pickup::PickupID::HealthRefill)].textureRect = sf::IntRect(0, 0, 40, 40);
	data[static_cast<int>(Pickup::PickupID::HealthRefill)].action = [](Commander& a) {
		a.repair(HEALTHPICKUPREPAIRVALUE); };

	/*data[static_cast<int>(Pickup::PickupID::FireSpread)].texture = TextureIDs::Entities;
	data[static_cast<int>(Pickup::PickupID::FireSpread)].textureRect = sf::IntRect(80, 64, 40, 40);*/
	data[static_cast<int>(Pickup::PickupID::FireSpread)].texture = TextureIDs::Pickups;
	data[static_cast<int>(Pickup::PickupID::FireSpread)].textureRect = sf::IntRect(40, 0, 40, 40);
	data[static_cast<int>(Pickup::PickupID::FireSpread)].action = std::bind(&Commander::increaseSpread, _1);

	/*data[static_cast<int>(Pickup::PickupID::FireRate)].texture = TextureIDs::Entities;
	data[static_cast<int>(Pickup::PickupID::FireRate)].textureRect = sf::IntRect(120, 64, 40, 40);*/
	data[static_cast<int>(Pickup::PickupID::FireRate)].texture = TextureIDs::Pickups;
	data[static_cast<int>(Pickup::PickupID::FireRate)].textureRect = sf::IntRect(80, 0, 40, 40);
	data[static_cast<int>(Pickup::PickupID::FireRate)].action = std::bind(&Commander::increaseFireRate, _1);

	return data;
}

std::vector<ParticleData> initializeParticleData()
{
	std::vector<ParticleData> data(static_cast<int>(Particle::Type::ParticleCount));
	data[static_cast<int>(Particle::Type::Exhaust)].color = sf::Color(148, 0, 211);
	//data[static_cast<int>(Particle::Type::Exhaust)].color = sf::Color(255, 255, 50);
	data[static_cast<int>(Particle::Type::Exhaust)].lifetime = sf::seconds(0.6f);

	data[static_cast<int>(Particle::Type::Smoke)].color = sf::Color(255, 20, 147);
	//data[static_cast<int>(Particle::Type::Smoke)].color = sf::Color(50, 50, 50);
	data[static_cast<int>(Particle::Type::Smoke)].lifetime = sf::seconds(4.0);

	return data;
}
