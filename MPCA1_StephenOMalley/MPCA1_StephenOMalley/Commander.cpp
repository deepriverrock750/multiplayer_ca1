#include "Commander.hpp"
#include "DataTables.hpp"
#include "Utility.hpp"
#include "Pickup.hpp"
#include "CommandQueue.hpp"
#include "ResourceHolder.hpp"
#include "SoundNode.hpp"

#include <iostream>

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>

#include <cmath>

namespace
{
	const std::vector<PlayerData> Table = initializePlayerData();
}

Commander::Commander(Type type, const TextureHolder & textures, const FontHolder & fonts)
	: Entity(Table[static_cast<int>(type)].hitpoints)
	, mType(type)
	, mSprite(textures.get(Table[static_cast<int>(type)].texture), Table[static_cast<int>(type)].textureRect)
	, mExplosion(textures.get(TextureIDs::Explosion))
	, mFireCommand()
	, mFireCountdown(sf::Time::Zero)
	, mIsFiring(false)
	, mShowExplosion(true)
	, mPlayedExplosionSound(false)
	, mSpawnedPickup(false)
	, mFireRateLevel(1)
	, mSpreadLevel(1)
	, mLives(3)
	, mDropPickupCommand()
	, mTravelledDistance(0.f)
	, mDirectionIndex(0)
	, mHealthDisplay(nullptr)
	, mLivesDisplay(nullptr)
{

	mExplosion.setFrameSize(sf::Vector2i(256, 256));
	mExplosion.setNumFrames(16);
	mExplosion.setDuration(sf::seconds(1));

	centreOrigin(mSprite);
	centreOrigin(mExplosion);

	mFireCommand.category = static_cast<int>(Category::SceneAirLayer);
	mFireCommand.action = [this, &textures](SceneNode& node, sf::Time)
	{
		createBullets(node, textures);
	};

	/*mMissileCommand.category = static_cast<int>(Category::SceneAirLayer);
	mMissileCommand.action = [this, &textures](SceneNode& node, sf::Time)
	{
		createProjectile(node, Projectile::ProjectileIDs::Missile, 0.f, 0.f, textures);
	};*/

	mDropPickupCommand.category = static_cast<int>(Category::SceneAirLayer);
	mDropPickupCommand.action = [this, &textures](SceneNode& node, sf::Time)
	{
		createPickup(node, textures);
	};

	std::unique_ptr<TextNode> healthDisplay(new TextNode(fonts, ""));
	mHealthDisplay = healthDisplay.get();
	attachChild(std::move(healthDisplay));

	std::unique_ptr<TextNode> livesDisplay(new TextNode(fonts, ""));
	livesDisplay->setPosition(0, 70);
	mLivesDisplay = livesDisplay.get();
	attachChild(std::move(livesDisplay));

	updateTexts();
}

unsigned int Commander::getCategory() const
{
	if (isAllied())
		return static_cast<int>(Category::Player1);
	else
		return static_cast<int>(Category::Player2);
}

sf::FloatRect Commander::getBoundingRect() const
{
	return getWorldTransform().transformRect(mSprite.getGlobalBounds());
}

int Commander::getLives()
{
	return mLives;
}

void Commander::lifeLosted()
{
	//if(isDestroyed())
		mLives -= 1;
		mExplosion.restart();
		mShowExplosion = true;
		mSpawnedPickup = false;
}

bool Commander::isMarkedForRemoval() const
{
	return isDestroyed() && (mExplosion.isFinished() || !mShowExplosion) && mLives == 0;
}

bool Commander::hasDied()
{
	return isDestroyed() && mExplosion.isFinished();
}

void Commander::remove()
{
	Entity::remove();
	mShowExplosion = false;
}


bool Commander::isAllied() const
{
	return mType == Type::Player1;
}

float Commander::getMaxSpeed() const
{
	return Table[static_cast<int>(mType)].speed;
}

void Commander::increaseFireRate()
{
	if (mFireRateLevel < 10)
		++mFireRateLevel;
}

void Commander::increaseSpread()
{
	if (mSpreadLevel < 3)
		++mSpreadLevel;
}

void Commander::fire()
{
	// Only ships with fire interval != 0 are able to fire
	if (Table[static_cast<int>(mType)].fireInterval != sf::Time::Zero)
		mIsFiring = true;
}

void Commander::playLocalSound(CommandQueue & commands, SoundEffectIDs effect)
{
	sf::Vector2f worldPosition = getWorldPosition();

	Command command;
	command.category = static_cast<int>(Category::SoundEffect);
	command.action = derivedAction<SoundNode>([effect, worldPosition](SoundNode& node, sf::Time)
	{
		node.playSound(effect, worldPosition);
	});
	commands.push(command);
}

void Commander::drawCurrent(sf::RenderTarget & target, sf::RenderStates states) const
{
	if (isDestroyed() && mShowExplosion)
		target.draw(mExplosion, states);
	else
		target.draw(mSprite, states);
}

void Commander::updateCurrent(sf::Time dt, CommandQueue & commands)
{
	// Update texts and roll animation
	updateTexts();
	updatePlayerAnimation();

	// Entity has been destroyed: Possibly drop pickup, mark for removal
	if (isDestroyed())
	{
		checkPickupDrop(commands);
		mExplosion.update(dt);

		// Play explosion sound only once
		if (!mPlayedExplosionSound)
		{
			SoundEffectIDs soundEffect = (randomInt(2) == 0) ? SoundEffectIDs::Explosion1 : SoundEffectIDs::Explosion2;
			playLocalSound(commands, soundEffect);

			mPlayedExplosionSound = true;
		}
		return;
	}

	// Check if bullets or missiles are fired
	checkProjectileLaunch(dt, commands);

	Entity::updateCurrent(dt, commands);
}

void Commander::checkPickupDrop(CommandQueue & commands)
{
	//if (!isAllied() && randomInt(3) == 0 && !mSpawnedPickup)
	if (!mSpawnedPickup)
		commands.push(mDropPickupCommand);

	mSpawnedPickup = true;
}

void Commander::checkProjectileLaunch(sf::Time dt, CommandQueue & commands)
{
	// Check for automatic gunfire, allow only in intervals
	if (mIsFiring && mFireCountdown <= sf::Time::Zero)
	{
		// Interval expired: We can fire a new bullet
		commands.push(mFireCommand);
		playLocalSound(commands, isAllied() ? SoundEffectIDs::AlliedGunfire : SoundEffectIDs::EnemyGunfire);
		mFireCountdown += Table[static_cast<int>(mType)].fireInterval / (mFireRateLevel + 1.f);
		mIsFiring = false;
	}
	else if (mFireCountdown > sf::Time::Zero)
	{
		// Interval not expired: Decrease it further
		mFireCountdown -= dt;
		mIsFiring = false;
	}
}

void Commander::createBullets(SceneNode & node, const TextureHolder & textures) const
{
	Projectile::ProjectileIDs type = isAllied() ? Projectile::ProjectileIDs::AlliedBullet : Projectile::ProjectileIDs::EnemyBullet;

	switch (mSpreadLevel)
	{
	case 1:
		/*createProjectile(node, type, -0.4f, 0.18f, textures);
		createProjectile(node, type, -0.35f, 0.18f, textures);
		createProjectile(node, type, -0.3f, 0.18f, textures);*/
		
		createProjectile(node, type, -0.2f, 0.18f, textures);

		/*createProjectile(node, type, -0.2f, 0.22f, textures);
		createProjectile(node, type, -0.4f, 0.22f, textures);*/
		break;

	case 2:
		createProjectile(node, type, -0.4f, 0.18f, textures);
		createProjectile(node, type, 0.f, 0.18f, textures);
		createProjectile(node, type, -0.2f, 0.18f, textures);
		break;

	case 3:
		createProjectile(node, type, -0.4f, 0.18f, textures);
		createProjectile(node, type, 0.f, 0.18f, textures);
		createProjectile(node, type, -0.2f, 0.18f, textures);
		break;
	}
}

void Commander::createProjectile(SceneNode & node, Projectile::ProjectileIDs type, float xOffset, float yOffset, const TextureHolder & textures) const
{
	int SpriteDirection = static_cast<int>(mSprite.getRotation());

	std::unique_ptr<Projectile> projectile(new Projectile(type, textures));
	
	sf::Vector2f offset(xOffset * mSprite.getGlobalBounds().width, yOffset * mSprite.getGlobalBounds().height);
	
	if (SpriteDirection == 0 || SpriteDirection == 180)
	{
		offset.x = (-(yOffset) * mSprite.getGlobalBounds().height);
		offset.y = (-(xOffset) * mSprite.getGlobalBounds().width);
	}
	

	/*std::cout << "OFFSET X: " << offset.x << "\nOFFSET Y: " << offset.y << "\nxOffeset: " << xOffset
		<< "\nyOffset: " << yOffset << "\nSGB Width: " << mSprite.getGlobalBounds().width
		<< "\nSGB Height: " << mSprite.getGlobalBounds().height << "\n "<< std::endl;*/

	sf::Vector2f velocity;
	float sign;

	
	switch (SpriteDirection)
	{
	case 0: //Left
		velocity.x = projectile->getMaxSpeed();
		velocity.y = 0;
		projectile->setRotation(90);
		sign = isAllied() ? +1.f : -1.f;
		break;
	case 90: //Down
		velocity.x = 0;
		velocity.y = projectile->getMaxSpeed();
		projectile->setRotation(180);
		sign = isAllied() ? +1.f : -1.f;
		break;
	case 180: //Right
		velocity.x = projectile->getMaxSpeed();
		velocity.y = 0;
		projectile->setRotation(270);
		sign = isAllied() ? -1.f : 1.f;
		break;
	case 270: //Up
		velocity.x = 0;
		velocity.y = projectile->getMaxSpeed();
		projectile->setRotation(0);
		sign = isAllied() ? -1.f : 1.f;
		break;
	default:
		break;
	}

	//sign = isAllied() ? -1.f : +1.f;
	projectile->setPosition(getWorldPosition() + offset * sign);
	projectile->setVelocity(velocity * sign);
	node.attachChild(std::move(projectile));
}

void Commander::createPickup(SceneNode & node, const TextureHolder & textures) const
{
	auto type = static_cast<Pickup::PickupID>(randomInt(static_cast<int>(Pickup::PickupID::TypeCount)));
	
	std::unique_ptr<Pickup> pickup(new Pickup(type, textures));
	pickup->setPosition(getWorldPosition());
	node.attachChild(std::move(pickup));
}

void Commander::updateTexts()
{
	mHealthDisplay->setString(std::to_string(getHitpoints()) + " HP");
	mHealthDisplay->setPosition(0.f, 50.f);
	mHealthDisplay->setRotation(-getRotation());

	/*if (mLivesDisplay)
	{
		if (mLives == 0)
			mLivesDisplay->setString("");
		else
			mLivesDisplay->setString("Lives: " + std::to_string(mLives));
	}*/
}

void Commander::updatePlayerAnimation()
{
	if (isAllied())
	{
		// Left
		if (getVelocity().x < 0.f)
			mSprite.setRotation(180.f);

		//Right
		else if (getVelocity().x > 0.f)
			mSprite.setRotation(0.f);

		// Face Up
		if (getVelocity().y < 0.f)
			mSprite.setRotation(270.f);
		//Face Down
		else if (getVelocity().y > 0.f)
			mSprite.setRotation(90.f);
	}
	else
	{
		// Left
		if (getVelocity().x < 0.f)
			mSprite.setRotation(0.f);

		//Right
		else if (getVelocity().x > 0.f)
			mSprite.setRotation(180.f);

		// Face Up
		if (getVelocity().y < 0.f)
			mSprite.setRotation(90.f);
		//Face Down
		else if (getVelocity().y > 0.f)
			mSprite.setRotation(270.f);
	}
}
