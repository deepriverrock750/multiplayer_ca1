#include "Wall.hpp"
#include "Category.hpp"
#include "Utility.hpp"
#include "ResourceHolder.hpp"

#include <SFML/Graphics/RenderTarget.hpp>
#include "SFML\System\Vector3.hpp"

Wall::Wall(sf::Vector2f size, sf::Vector2f position)
	: Entity(1)
	, mRectangle()
{
	sf::Color fillBlack = sf::Color(31, 31, 31, 1);
	sf::Color outlineBlack = sf::Color(58, 58, 58, 1);

	mRectangle.setSize(size);
	mRectangle.setFillColor(sf::Color(58, 58, 58));
	mRectangle.setOutlineColor(sf::Color(31, 31, 31));
	mRectangle.setOutlineThickness(7);
	mRectangle.setPosition(position);
	//centreOrigin(mRectangle);

}

unsigned int Wall::getCategory() const
{
	return static_cast<int>(Category::Wall);
}

sf::FloatRect Wall::getBoundingRect() const
{
	return getWorldTransform().transformRect(mRectangle.getGlobalBounds());
}

void Wall::drawCurrent(sf::RenderTarget & target, sf::RenderStates states) const
{
	target.draw(mRectangle, states);
}
