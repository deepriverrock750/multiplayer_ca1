#include "LevelState.hpp"
#include "Button.hpp"
#include "Utility.hpp"
#include "ResourceHolder.hpp"
#include "MusicPlayer.hpp"

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/View.hpp>

LevelState::LevelState(StateStack & stack, Context context)
	: State(stack, context)
	, mGUIContainer()
	, mLevel(*context.level)
{
	sf::Texture& texture = context.textures->get(TextureIDs::LevelSelect);
	mBackgroundSprite.setTexture(texture);

	auto playLevel1Button = std::make_shared<GUI::Button>(context);
	playLevel1Button->setPosition(150, 250);
	playLevel1Button->setText("Level 1");
	playLevel1Button->setCallback([this]()
	{
		//mLevel = context.level->SetCurrentLevel(LevelIDs::Level1);
		mLevel.SetCurrentLevel(LevelIDs::Level1);
		requestStackPop();
		requestStackPush(StateIDs::Game);
	});

	auto playLevel2Button = std::make_shared<GUI::Button>(context);
	playLevel2Button->setPosition(575, 250);
	playLevel2Button->setText("Level 2");
	playLevel2Button->setCallback([this]()
	{
		mLevel.SetCurrentLevel(LevelIDs::Level2);
		requestStackPop();
		requestStackPush(StateIDs::Game);
	});

	auto playLevel3Button = std::make_shared<GUI::Button>(context);
	playLevel3Button->setPosition(1000, 250);
	playLevel3Button->setText("Level 3");
	playLevel3Button->setCallback([this]()
	{
		mLevel.SetCurrentLevel(LevelIDs::Level3);
		requestStackPop();
		requestStackPush(StateIDs::Game);
	});

	auto backButton = std::make_shared<GUI::Button>(context);
	backButton->setPosition(1100, 675);
	backButton->setText("Back");
	backButton->setCallback([this]()
	{
		requestStackPop();
		requestStackPush(StateIDs::Menu);
	});

	mGUIContainer.pack(playLevel1Button);
	mGUIContainer.pack(playLevel2Button);
	mGUIContainer.pack(playLevel3Button);
	mGUIContainer.pack(backButton);

	//Play the menu music
}

void LevelState::draw()
{
	sf::RenderWindow& window = *getContext().window;

	window.setView(window.getDefaultView());

	window.draw(mBackgroundSprite);
	window.draw(mGUIContainer);
}

bool LevelState::update(sf::Time dt)
{
	return true;
}

bool LevelState::handleEvent(const sf::Event & event)
{
	mGUIContainer.handleEvent(event);
	return false;
}
