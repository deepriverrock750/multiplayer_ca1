#pragma once
#include "Entity.hpp"
#include "ResourceIdentifiers.hpp"
#include "Command.hpp"
#include "Projectile.hpp"
#include "TextNode.hpp"
#include "Animation.hpp"

#include "SFML/Graphics/Sprite.hpp"

class Commander : public Entity
{
public:
	enum class Type { Player1, Player2, TypeCount };

public:
	Commander(Type type, const TextureHolder& textures, const FontHolder& fonts);
	virtual unsigned int getCategory() const;
	virtual sf::FloatRect getBoundingRect() const;
	int getLives();
	void lifeLosted();
	virtual void remove();
	virtual bool isMarkedForRemoval() const;
	bool hasDied();
	bool isAllied() const;
	float getMaxSpeed() const;


	void increaseFireRate();
	void increaseSpread();

	void fire();
	void playLocalSound(CommandQueue& commands, SoundEffectIDs effect);

private:
	virtual void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;
	virtual void updateCurrent(sf::Time dt, CommandQueue& commands);
	void checkPickupDrop(CommandQueue& commands);
	void checkProjectileLaunch(sf::Time dt, CommandQueue& commands);

	void createBullets(SceneNode& node, const TextureHolder& textures) const;
	void createProjectile(SceneNode& node, Projectile::ProjectileIDs type, float xOffset, float yOffset, const TextureHolder& textures) const;
	void createPickup(SceneNode& node, const TextureHolder& textures) const;
	void updateTexts();
	void updatePlayerAnimation();

private:
	Type mType;
	sf::Sprite mSprite;
	Animation mExplosion;
	Command mFireCommand;
	sf::Time mFireCountdown;
	bool mIsFiring;
	bool mShowExplosion;
	bool mPlayedExplosionSound;
	bool mSpawnedPickup;

	int mFireRateLevel;
	int mSpreadLevel;
	int mLives;

	Command mDropPickupCommand;
	float mTravelledDistance;
	std::size_t mDirectionIndex;
	TextNode* mHealthDisplay;
	TextNode* mLivesDisplay;


};

