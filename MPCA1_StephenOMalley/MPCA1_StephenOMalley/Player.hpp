#pragma once
#include "Command.hpp"
#include "SFML/Window/Event.hpp"
#include <map>

class CommandQueue;

class Player
{
public:
	enum class Action{P1MoveLeft, P1MoveRight, P1MoveUp, P1MoveDown, P1Fire, 
		P2MoveLeft, P2MoveRight, P2MoveUp, P2MoveDown, P2Fire, ActionCount};
	enum class MissionStatus{MissionRunning, MissionSuccess, MissionFailure};

public:
	Player();
	void handleEvent(const sf::Event& event, CommandQueue& commands);
	void handleRealtimeInput(CommandQueue& commands);
	void assignKey(Action action, sf::Keyboard::Key key);
	sf::Keyboard::Key getAssignedKey(Action action) const;

	void setMissionStatus(MissionStatus status);
	MissionStatus getMissionStatus() const;

private:
	void initializeActions();
	static bool isRealtimeAction(Action action);

private:
	std::map<sf::Keyboard::Key, Action> mKeyBinding;
	std::map<Action, Command> mActionBinding;
	std::map<sf::Keyboard::Key, Action> mKeyBindingPlayer2;
	std::map<Action, Command> mActionBindingPlayer2;
	MissionStatus mCurrentMissionStatus;

};