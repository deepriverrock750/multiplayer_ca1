#pragma once
const int PLAYERHEALTH = 100;
const int HEALTHPICKUPREPAIRVALUE = 40;
//const int MISSILEPICKUPAMMOVALUE = 3;
const int FIRERATELEVEL = 1;
const int MAXFIRERATELEVEL = 10;
const int MAXSPREADLEVEL = 3;
const int SPREADLEVEL = 1;
//const int MISSILEAMMO = 2;
const float M_PI = 3.14159f;
//const float APPROACHRATE = 200;